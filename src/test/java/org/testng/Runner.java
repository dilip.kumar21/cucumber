package org.testng;

import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(features="src/test/resources/testngcucumber", glue = "org.testng")
public class Runner extends AbstractTestNGCucumberTests{
	
	
	

}
