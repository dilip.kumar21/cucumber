package org.testng;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class Stepdefn {
	
	WebDriver driver = new ChromeDriver();

	@Given("User has to launch the web application demowebshop.tricentis.com through browser")
	public void user_has_to_launch_the_web_application_demowebshop_tricentis_com_through_browser() {
		driver.get("https://demowebshop.tricentis.com/");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(2));

	}

	@When("User click the login button")
	public void user_click_the_login_button() {
		driver.findElement(By.linkText("Log in")).click();

	}

	@When("User has to Enter the valid user name and valild password")
	public void user_has_to_enter_the_valid_user_name_and_valild_password() {
		driver.findElement(By.id("Email")).sendKeys("pdkumar241095@gmail.com");
		driver.findElement(By.id("Password")).sendKeys("Dilipkd#2124");

	}

	@When("User has to click the Login button")
	public void user_has_to_click_the_login_button() {
		driver.findElement(By.xpath("(//input[@type='submit'])[2]")).click();

	}

	@Then("User should navigate to shopping page")
	public void user_should_navigate_to_shopping_page()

	{
	System.out.println(driver.findElement(By.xpath("//div[@class='topic-html-content']")).getText());
	driver.close();
	}

}
