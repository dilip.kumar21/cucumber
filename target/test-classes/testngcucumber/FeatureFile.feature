Feature: Validate the Login functionality of DemoWebshop Web Application

  Scenario: Validate the Login functionality with Valid username and password
    Given User has to launch the web application demowebshop.tricentis.com through browser
    When User click the login button
    And User has to Enter the valid user name and valild password 
    And User has to click the Login button
    Then User should navigate to shopping page
